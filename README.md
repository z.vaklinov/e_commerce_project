# E_Commerce_Project

This is a follow through project which aims to build on the final team project at Telerik Academy:

https://gitlab.com/_-35-group_7/telerik-academy-35_team-6_photo-contest


It's an abstract E-commerce application. It follows a monolith N-layer architecture, which follows clean architecture principles and especially Domain Driven Design.

It's being developed as abstract as possible since it doesn't follow a specific business need and it servers as a mere template.

## NOTE:

- Implementing Domain Driven Design is not a necessity, as for it to be successfully implemented the entire team (software and business) should be aware of how it is used. If the majority of your colleagues do not know what DDD means, then it's better if it is not used. The main ideology of DDD is to set a standart for internal communication of the project.

- I have decided to implement DDD, because it is my choice to learn the principle. If one would not implement DDD, then the bottom layer of your architecture should be Application Layer.

- If you are developing your own practice project, or a small commercial project, doing DDD would simply be an overkill. As you can see in the top link (my final team project at Telerik Academy), such an architecture allows for the successfull development of a working solution for a small project. DDD is a step into big commercial projects in today's world, where we have many complext technologies that need integration.


## References

I have gathered various sources of information and materials which I have used for this implementation. However, the biggest source of all are the consultations of my friends, who are in the software development industry for years.

- [DDD - How I put it all together;](https://herbertograca.com/2017/11/16/explicit-architecture-01-ddd-hexagonal-onion-clean-cqrs-how-i-put-it-all-together/?fbclid=IwAR0tOJy_c-hv3TRmyaYH5aZm5X1_78ZNy8zfIyIHS-nwsIJ5AL8ypSWnr3I)

- The Evans Classification - In his book, Eric Evans creates a classification of the different kinds of domain objects that you're likely to run into;

- [Azure application architecture fundamentals](https://docs.microsoft.com/en-us/azure/architecture/guide/?fbclid=IwAR0V-9JoLt9lY1WWVK77DSGOHLHUa7cZC7qAA5r9-PQK7HrcvM1q6dDpF88)

[- Github, kgrzybek/modular-monolith-with-ddd](https://github.com/kgrzybek/modular-monolith-with-ddd?fbclid=IwAR2O-idugqrmC0eNqw3xFpPrlHq1IXI7M7DIethpY8AbvmVOA6LnaTLEILE#2-Domain)

[- Martin Fowler's blog on DDD](https://martinfowler.com/bliki/DomainDrivenDesign.html?fbclid=IwAR1nx5uvcOpRPbYpmnRwuwHgNVQC0z8wrA3ysMAtRNMN-A0rOaRTUresdZI);

[- The CQRS pattern](https://docs.microsoft.com/en-us/azure/architecture/patterns/cqrs?fbclid=IwAR0ZCJg7GMv-tUc70qJ5fcpDRXFy-V2HYxexlY_Sj54HsGOKiclZtDy_WHg);

[- jbogard/MediatR Library](https://github.com/jbogard/MediatR?fbclid=IwAR2_VkqVPQSu5qk14kpFd_r8HVPH99m7XtzoKGkX6yuTOl_39SERARsF_e8);

[- Domain Events - design and implementation](https://docs.microsoft.com/en-us/dotnet/architecture/microservices/microservice-ddd-cqrs-patterns/domain-events-design-implementation);

[- dotnet-architecture/eShopOnContainers](https://github.com/dotnet-architecture/eShopOnContainers/tree/dev/src/Services/Ordering/Ordering.Domain/AggregatesModel/OrderAggregate);

## Project status
Currently in development.
