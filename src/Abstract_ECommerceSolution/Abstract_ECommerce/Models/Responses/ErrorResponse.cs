﻿namespace EC.Presentation.API.Models.Responses
{
    public class ErrorResponse
    {
        public string Message { get; set; } = string.Empty;
    }
}
