﻿namespace EC.Presentation.API.Models.Responses
{
    public class CreateReviewResponse 
    {
        public Guid ProductId { get; set; }

        public int Rating { get; set; }

        public string Text { get; set; } = string.Empty;
    }
}
