﻿namespace EC.Presentation.API.Models.Responses
{
    public class CreateCartResponse
    {
        public Guid BuyerId { get; set; }

        public Guid OrderId { get; set; }
    }
}
