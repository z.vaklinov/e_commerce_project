﻿namespace EC.Presentation.API.Models.Responses
{
    public class CreateProductResponse
    {
        public Guid CategoryId { get; set; }

        public string Name { get; set; } = string.Empty;

        public string Description { get; set; } = string.Empty;

        public int Rating { get; set; }

        public int Units { get; set; }
    }
}
