﻿using AutoMapper;
using EC.Core.Application.Features.Carts.Commands.Create;
using EC.Core.Application.Features.Products.Commands.Create;
using EC.Core.Application.Models.DataTransfer;
using EC.Presentation.API.Models.Requests;
using EC.Presentation.API.Models.Responses;

namespace EC.Presentation.API.Models
{
    public class RequestResponseMappingProfiler : Profile
    {
        public RequestResponseMappingProfiler()
        {
            CreateMap<CreateProductRequest, CreateProductCommand>().ReverseMap();
            CreateMap<CreateProductResponse, ProductDto>().ReverseMap();
            CreateMap<CreateProductCommand, CreateProductResponse>().ReverseMap();
            CreateMap<CreateProductRequest, CreateProductResponse>().ReverseMap();

            CreateMap<CreateCartRequest, CreateCartCommand>().ReverseMap();

            CreateMap<CreateCartResponse, CartDto>().ReverseMap();

            //CreateMap<CreateProductCommand, CreateProductResponse>().ReverseMap();
            //CreateMap<CreateProductRequest, CreateProductResponse>().ReverseMap();
        }
    }
}
