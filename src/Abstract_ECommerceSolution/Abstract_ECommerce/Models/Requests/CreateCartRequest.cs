﻿namespace EC.Presentation.API.Models.Requests
{
    public class CreateCartRequest
    {
        public Guid BuyerId { get; set; }

        public Guid OrderId { get; set; }
    }
}
