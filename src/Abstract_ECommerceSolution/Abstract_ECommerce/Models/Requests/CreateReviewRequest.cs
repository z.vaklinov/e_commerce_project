﻿namespace EC.Presentation.API.Models.Requests
{
    public class CreateReviewRequest
    {
        public Guid ProductId { get; set; }

        public byte Rating { get; set; }

        public string Text { get; set; } = string.Empty;
    }
}
