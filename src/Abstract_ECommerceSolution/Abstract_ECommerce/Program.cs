using EC.Core.Application;
using EC.Core.Application.Features;
using EC.Core.Application.Features.Commands.Products.Create;
using EC.Core.Application.Features.Reviews.Commands.Create;
using EC.Infrastructure.Logging;
using EC.Infrastructure.Persistence;
using EC.Presentation.API.Middlewares;
using EC.Presentation.API.Models;
using MediatR;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.RegisterApplicationDependencies();

// This registers the whole assembly so including the CreateReviewCommand is enough for all commands.
builder.Services.AddMediatR(typeof(CreateReviewCommand).Assembly);

builder.Services.RegisterLoggingDependencies();
builder.Services.AddAutoMapper(typeof(FeaturesMappingProfiler), typeof(RequestResponseMappingProfiler));
builder.Services.RegisterPersistenceDependencies(
    builder.Configuration.GetConnectionString("MsSQLDbConnection"));

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseMiddleware<ExceptionHandlingMiddleware>();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();



