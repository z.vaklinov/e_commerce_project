﻿using EC.Infrastructure.Persistence.MsSQL;
using Microsoft.EntityFrameworkCore;

namespace EC.Presentation.API
{
    public class DbContextFactory
    {
        public MsSQLDbContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var connectionString = configuration.GetConnectionString("MsSQLDbConnection");
            var optionsBuilder = new DbContextOptionsBuilder<MsSQLDbContext>();
            optionsBuilder.UseSqlServer(connectionString);

            return new MsSQLDbContext(optionsBuilder.Options);
        }
    }
}
