﻿using EC.Core.Domain.Abstraction.Models;
using EC.Presentation.API.Models.Responses;
using Newtonsoft.Json;

namespace EC.Presentation.API.Middlewares
{
    public class ExceptionHandlingMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionHandlingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (DomainException ex)
            {
                await HandleExceptionAsync(httpContext, ex);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(httpContext, ex);
            }
        }

        private async Task HandleExceptionAsync(HttpContext httpContext, DomainException exception)
        {
            httpContext.Response.ContentType = "application/json";
            httpContext.Response.StatusCode = StatusCodes.Status400BadRequest;

            await httpContext.Response.Body.FlushAsync();

            var result = JsonConvert.SerializeObject(new ErrorResponse
            {
                Message = exception.Message 
            });

            await httpContext.Response.WriteAsync(result);
        }

        private async Task HandleExceptionAsync(HttpContext httpContext, Exception exception)
        {
            httpContext.Response.ContentType = "application/json";
            httpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;

            await httpContext.Response.Body.FlushAsync();

            var result = JsonConvert.SerializeObject(new ErrorResponse
            {
                Message = exception.Message
            });

            await httpContext.Response.WriteAsync(result);
        }
    }
}
