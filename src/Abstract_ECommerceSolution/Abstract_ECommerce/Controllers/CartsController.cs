﻿using AutoMapper;
using EC.Core.Application.Features.Carts.Commands.Create;
using EC.Presentation.API.Models.Requests;
using EC.Presentation.API.Models.Responses;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace EC.Presentation.API.Controllers
{
    public class CartsController : BaseController
    {
        public CartsController(IMediator mediator, IMapper mapper)
            : base(mediator, mapper)
        {

        }

        [HttpPost]
        public async Task<ActionResult<CreateCartResponse>> CreateProduct(
            [FromBody] CreateCartRequest request)
        {
            // chage with automapper
            var command = _mapper.Map<CreateCartCommand>(request);

            var result = await Mediator.Send(command);

            // change with automapper
            var response = _mapper.Map<CreateCartResponse>(result);

            return this.StatusCode(StatusCodes.Status200OK, response);
        }
    }
}
