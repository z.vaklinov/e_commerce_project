﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace EC.Presentation.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public abstract class BaseController : ControllerBase
    {
        protected readonly IMapper _mapper;

        public BaseController(IMediator mediator, IMapper mapper)
        {
            Mediator = mediator;
            _mapper = mapper;
        }

        protected IMediator Mediator { get; }
    }
}
