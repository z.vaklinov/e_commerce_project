﻿using AutoMapper;
using EC.Core.Application.Features.Products.Commands.Create;
using EC.Presentation.API.Models.Requests;
using EC.Presentation.API.Models.Responses;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace EC.Presentation.API.Controllers
{
    public class ProductsController : BaseController
    {
        public ProductsController(IMediator mediator, IMapper mapper)
            : base(mediator, mapper)
        {
        }

        [HttpPost]
        public async Task<ActionResult<CreateProductResponse>> CreateProduct(
            [FromBody] CreateProductRequest request)
        {
            // chage with automapper
            var command = _mapper.Map<CreateProductCommand>(request);

            var result = await Mediator.Send(command);

            // change with automapper
            var response = _mapper.Map<CreateProductResponse>(result);

            return this.StatusCode(StatusCodes.Status200OK, response);
        }
    }
}
