﻿using AutoMapper;
using EC.Core.Application.Features.Commands.Reviews.Create;
using EC.Presentation.API.Models.Requests;
using EC.Presentation.API.Models.Responses;
using MediatR;
using Microsoft.AspNetCore.Mvc;


namespace EC.Presentation.API.Controllers
{
    public class ReviewsController : BaseController
    {

        public ReviewsController(IMediator mediator, IMapper mapper)
            : base(mediator, mapper)
        {
        }

        [HttpPost]
        public async Task<ActionResult<CreateReviewResponse>> CreateReview(
            [FromBody] CreateReviewRequest request)
        {
            // chage with automapper
            var command = new CreateReviewCommand(
                request.ProductId, 
                request.Rating, 
                request.Text);

            var result = await Mediator.Send(command);

            // chage with automapper
            var response = new CreateReviewResponse
            {
                ProductId = result.ProductId,
                Rating = result.Rating,
                Text = result.Text,
            };

            return this.StatusCode(StatusCodes.Status200OK, response);
        }
    }
}
