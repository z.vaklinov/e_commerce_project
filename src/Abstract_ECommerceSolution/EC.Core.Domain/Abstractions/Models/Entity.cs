﻿using EC.Core.Domain.Abstraction.Interfaces;
using FluentValidation;

namespace EC.Core.Domain.Abstraction.Models
{
    public abstract class Entity<Guid> : 
        Validatable, 
        IDisposable
    {
        private bool _disposedValue;
        private List<IDomainEvent> _domainEvents;

        protected Entity(
            Func<Guid> identifierFactory,
            Func<IValidator> validatorFactory = null)
            : this(validatorFactory)
        {
            Id = identifierFactory();
        }

        protected Entity(Func<IValidator> validatorFactory = null)
            : base(validatorFactory)
        {
            _domainEvents = new List<IDomainEvent>();
            CreatedOn = DateTimeOffset.UtcNow;
            MarkAsModified(CreatedOn);
        }

        public IReadOnlyCollection<IDomainEvent> DomainEvents => _domainEvents;

        public Guid Id { get; }

        public DateTimeOffset CreatedOn { get; }

        public DateTimeOffset ModifiedOn { get; }

        protected IDomainEvent AddDomainEvent(IDomainEvent @event)
        {
            _domainEvents.Add(@event);
            return @event;
        }

        protected IDomainEvent RemoveDomainEvent(IDomainEvent @event)
        {
            _domainEvents.Remove(@event);
            return @event;
        }

        public void MarkAsModified(DateTimeOffset? modificationTime = null)
        {
            if (modificationTime == null)
            {
                modificationTime = DateTimeOffset.UtcNow;
            }
            else
            {
                modificationTime = modificationTime.Value;
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    // Dispose managed resources here, if any
                }

                // Dispose unmanaged resources here, if any
                // Set large fields to null
                _disposedValue = true;
            }
        }

        // Finalizer (destructor) if unmanaged resources are used
        ~Entity()
        {
            Dispose(disposing: false);
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
