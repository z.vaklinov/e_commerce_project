﻿using EC.Core.Domain.Abstraction.Interfaces;

namespace EC.Core.Domain.Abstraction.Models
{
    public abstract class DomainEvent<TOrigin> : IDomainEvent
        where TOrigin : Entity<Guid>
    {
        protected DomainEvent(Entity<Guid> origin)
        {
            Origin = origin;
        }

        public Entity<Guid> Origin { get; }
    }
}
