﻿using FluentValidation;

namespace EC.Core.Domain.Abstraction.Models
{
    public abstract class Aggregate<TIdentifier> : Entity<TIdentifier>
    {
        protected Aggregate(
            Func<TIdentifier> identifierFactory,
            Func<IValidator> validatorFactory = null)
            : base(identifierFactory, validatorFactory)
        {
        }

        protected Aggregate(Func<IValidator> validatorFactory = null)
            : base(validatorFactory)
        {
        }
    }
}
