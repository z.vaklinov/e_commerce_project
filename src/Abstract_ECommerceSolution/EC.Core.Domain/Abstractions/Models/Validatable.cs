﻿using EC.Core.Domain.Abstraction.Services;
using FluentValidation;
using FluentValidation.Internal;

namespace EC.Core.Domain.Abstraction.Models
{
    public abstract class Validatable
    {
        private readonly Func<IValidator> _validatorFactory;

        public Validatable(Func<IValidator> validatorFactory = null)
        {
            _validatorFactory = validatorFactory ?? CreateDefault;
        }

        protected void ValidateProperty(string property)
        {
            var context = new ValidationContext<Validatable>(
                this,
                new PropertyChain(),
                new MemberNameValidatorSelector(new[] { property }));

            var validator = _validatorFactory();
            var result = validator.Validate(context);

            if (result.IsValid == false)
            {
                var error = result.Errors.FirstOrDefault();
                throw new DomainException(error.PropertyName, error.ErrorMessage);
            }
        }

        private IValidator CreateDefault()
        {
            return new DomainValidationService<Validatable>();
        }
    }
}
