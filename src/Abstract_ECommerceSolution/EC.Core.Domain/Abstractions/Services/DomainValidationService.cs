﻿using EC.Core.Domain.Abstraction.Models;
using FluentValidation;

namespace EC.Core.Domain.Abstraction.Services
{
    public class DomainValidationService<TValidatable> : AbstractValidator<TValidatable>
        where TValidatable : Validatable
    {
        public DomainValidationService()
        {
            CascadeMode = CascadeMode.Stop;
        }
    }
}
