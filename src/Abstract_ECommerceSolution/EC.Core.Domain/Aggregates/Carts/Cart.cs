﻿using EC.Core.Domain.Abstraction.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EC.Core.Domain.Aggregates.Carts
{
    public class Cart : Aggregate<Guid>
    {
        private readonly List<CartItem> _cartItems;
        private Guid _buyerId;
        private Guid _orderId;

        public Cart(Guid buyerId, Guid orderId)
            : this()
        {
            BuyerId = buyerId;
            OrderId = orderId;

            AddDomainEvent(new CartCreatedDomainEvent(this));
        }

        protected Cart()
            : base(() => new CartValidationService())
        {
            _cartItems = new List<CartItem>();
        }

        public Guid BuyerId
        {
            get => _buyerId;
            private set
            {
                _buyerId = value;
                ValidateProperty(nameof(BuyerId));
            }
        }

        public Guid OrderId
        {
            get => _orderId;
            private set
            {
                _orderId = value;
                ValidateProperty(nameof(OrderId));
            }
        }

        public IReadOnlyCollection<CartItem> CartItems => _cartItems;

        public CartItem AddCartItem(Guid productId, Guid cartId, int quantity)
        {
            var cartItem = new CartItem(productId, cartId, quantity);
            _cartItems.Add(cartItem);

            return cartItem;
        }
    }
}
