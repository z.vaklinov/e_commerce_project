﻿using EC.Core.Domain.Abstraction.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EC.Core.Domain.Aggregates.Carts
{
    public class CartCreatedDomainEvent : DomainEvent<Cart>
    {
        public CartCreatedDomainEvent(Cart cart)
            : base(cart)
        {

        }
    }
}
