﻿using EC.Core.Domain.Abstraction.Services;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EC.Core.Domain.Aggregates.Carts
{
    public class CartValidationService : DomainValidationService<Cart>
    {
        public CartValidationService()
        {
            RuleFor(x => x.CartItems).NotEmpty();
        }
    }
}
