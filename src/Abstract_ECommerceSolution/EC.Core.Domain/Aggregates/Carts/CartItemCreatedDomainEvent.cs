﻿using EC.Core.Domain.Abstraction.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EC.Core.Domain.Aggregates.Carts
{
    public class CartItemCreatedDomainEvent : DomainEvent<Cart>
    {
        public CartItemCreatedDomainEvent(CartItem cartItem)
            : base(cartItem)
        {

        }
    }
}
