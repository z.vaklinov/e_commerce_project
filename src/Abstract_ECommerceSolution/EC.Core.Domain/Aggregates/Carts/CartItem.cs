﻿using EC.Core.Domain.Abstraction.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EC.Core.Domain.Aggregates.Carts
{
    public class CartItem : Entity<Guid>
    {
        private Guid _orderItemId;
        private Guid _cartId;
        private int _units;

        public CartItem(Guid orderItemId, Guid cartId, int units)
            : this()
        {
            OrderItemId = orderItemId;
            CartId = cartId;
            Units = units;

            AddDomainEvent(new CartItemCreatedDomainEvent(this));
        }

        protected CartItem()
            : base()
        {
        }

        public Guid OrderItemId
        {
            get => _orderItemId;
            private set
            {
                _orderItemId = value;
                ValidateProperty(nameof(OrderItemId));
            }
        }

        public Guid CartId
        {
            get => _cartId;
            private set
            {
                _cartId = value;
                ValidateProperty(nameof(CartId));
            }
        }

        public int Units
        {
            get => _units;
            private set
            {
                _units = value;
                ValidateProperty(nameof(Units));
            }
        }
    }
}
