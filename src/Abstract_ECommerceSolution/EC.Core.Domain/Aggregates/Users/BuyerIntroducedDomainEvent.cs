﻿using EC.Core.Domain.Abstraction.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EC.Core.Domain.Aggregates.Users
{
    public class BuyerIntroducedDomainEvent : DomainEvent<Buyer>
    {
        public BuyerIntroducedDomainEvent(Buyer buyer) 
            : base(buyer)
        {

        }
    }
}
