﻿using EC.Core.Domain.Abstraction.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EC.Core.Domain.Aggregates.Users
{
    public class FullName : ValueObject
    {
        private string _firstName;
        private string _lastName;

        public FullName(string firstName, string lastName)
            : this()
        {
            FirstName = firstName;
            LastName = lastName;

        }

        protected FullName()
            : base()
        {
        }

        public string FirstName
        {
            get => _firstName;
            private set
            {
                _firstName = value;
                ValidateProperty(nameof(FirstName));
            }
        }

        public string LastName
        {
            get => _lastName;
            private set
            {
                _lastName = value;
                ValidateProperty(nameof(LastName));
            }
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            throw new NotImplementedException();
        }
    }
}
