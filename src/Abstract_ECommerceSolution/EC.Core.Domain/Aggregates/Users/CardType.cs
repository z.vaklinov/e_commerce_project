﻿
namespace EC.Core.Domain.Aggregates.Users
{
    public enum CardType
    {
        Visa = 1,
        MasterCard = 2,
        AmericanExpress = 3,
        Discover = 4
    }
}
