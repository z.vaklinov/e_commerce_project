﻿using EC.Core.Domain.Abstraction.Models;

namespace EC.Core.Domain.Aggregates.Users
{
    public class Buyer : Aggregate<Guid>
    {
        private FullName _fullName;
        private PhoneNumber _phoneNumber;

        private readonly List<PaymentMethod> _paymentMethods;

        public Buyer(FullName fullName, PhoneNumber phoneNumber)
            : this()
        {
            FullName = fullName;
            PhoneNumber = phoneNumber;

            AddDomainEvent(new BuyerIntroducedDomainEvent(this));
        }

        protected Buyer()
            : base(() => new BuyerValidationService())
        {
            _paymentMethods = new List<PaymentMethod>();
        }

        public FullName FullName
        {
            get => _fullName;
            private set
            {
                _fullName = value;
                ValidateProperty(nameof(FullName));
            }
        }

        public PhoneNumber PhoneNumber
        {
            get => _phoneNumber;
            private set
            {
                _phoneNumber = value;
                ValidateProperty(nameof(PhoneNumber));
            }
        }

        public IReadOnlyCollection<PaymentMethod> PaymentMethod => _paymentMethods;


        public PaymentMethod VerifyOrAddPaymentMethod(string alias, string cardNumber,
        string securityNumber, string cardHolderName, DateTime expiration, CardType cardType, int orderId)

        {
            var existingPayment = _paymentMethods
                .SingleOrDefault(p => p.IsEqualTo(cardType, cardNumber, expiration));

            if (existingPayment != null)
            {
                AddDomainEvent(new BuyerAndPaymentMethodVerifiedDomainEvent(existingPayment, this, orderId));

                return existingPayment;
            }

            var payment = new PaymentMethod(alias, cardNumber, securityNumber, cardHolderName, expiration, cardType);

            _paymentMethods.Add(payment);

            AddDomainEvent(new BuyerAndPaymentMethodVerifiedDomainEvent(payment, this, orderId));

            return payment;
        }
    }
}
