﻿using EC.Core.Domain.Abstraction.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EC.Core.Domain.Aggregates.Users
{
    public class BuyerAndPaymentMethodVerifiedDomainEvent : DomainEvent<Buyer>
    {
        public BuyerAndPaymentMethodVerifiedDomainEvent(PaymentMethod paymentMethod, Buyer buyer, int orderId)
            : base(paymentMethod)
        {
            Buyer = buyer;
            OrderId = orderId;
        }

        public Buyer Buyer { get; private set; }
        public int OrderId { get; private set; }
    }
}
