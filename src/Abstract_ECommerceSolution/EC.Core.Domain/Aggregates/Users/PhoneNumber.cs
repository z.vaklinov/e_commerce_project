﻿using EC.Core.Domain.Abstraction.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EC.Core.Domain.Aggregates.Users
{
    public class PhoneNumber : ValueObject
    {
        private string _countryCode;
        private int _number;

        public PhoneNumber(string countryCode, int number)
            : this()
        {
            CountryCode = countryCode;
            Number = number;

        }

        protected PhoneNumber()
            : base()
        {
        }

        public string CountryCode
        {
            get => _countryCode;
            private set
            {
                _countryCode = value;
                ValidateProperty(nameof(CountryCode));
            }
        }

        public int Number
        {
            get => _number;
            private set
            {
                _number = value;
                ValidateProperty(nameof(Number));
            }
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            throw new NotImplementedException();
        }
    }
}
