﻿using EC.Core.Domain.Abstraction.Models;
using EC.Core.Domain.Abstraction.Services;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EC.Core.Domain.Aggregates.Users
{
    public class PaymentMethodValidationService : DomainValidationService<PaymentMethod>
    {
        public PaymentMethodValidationService()
        {
            RuleFor(x => x.CardNumber)
                .CreditCard();
            RuleFor(x => x.SecurityNumber)
                .Length(3, 3);
            RuleFor(x => x.CardHolderName)
                .NotEmpty();
        }
    }
}
