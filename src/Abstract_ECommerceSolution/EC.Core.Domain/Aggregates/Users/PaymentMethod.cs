﻿using EC.Core.Domain.Abstraction.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EC.Core.Domain.Aggregates.Users
{
    public class PaymentMethod : Entity<Guid>
    {
        private string _alias;
        private string _cardNumber;
        private string _securityNumber;
        private string _cardHolderName;
        private DateTime _expiration;
        private CardType _cardType;

        public PaymentMethod(string alias, string cardNumber, string securityNumber,
            string cardHolderName, DateTime expiration, CardType cardType)
            : this()
        {
            Alias = alias;
            CardNumber = cardNumber;
            SecurityNumber = securityNumber;
            CardHolderName = cardHolderName;
            Expiration = expiration;
            CardType = cardType;

            AddDomainEvent(new BuyerPaymentMethodChosenDomainEvent(this));
        }

        protected PaymentMethod()
            : base()
        {
        }

        public string Alias
        {
            get => _alias;
            private set
            {
                _alias = value;
                ValidateProperty(nameof(Alias));
            }
        }

        public string CardNumber
        {
            get => _cardNumber;
            private set
            {
                _cardNumber = value;
                ValidateProperty(nameof(CardNumber));
            }
        }

        public string SecurityNumber
        {
            get => _securityNumber;
            private set
            {
                _securityNumber = value;
                ValidateProperty(nameof(SecurityNumber));
            }
        }

        public string CardHolderName
        {
            get => _cardHolderName;
            private set
            {
                _cardHolderName = value;
                ValidateProperty(nameof(CardHolderName));
            }
        }

        public DateTime Expiration
        {
            get => _expiration;
            private set
            {
                _expiration = value;
                ValidateProperty(nameof(Expiration));
            }
        }

        public CardType CardType
        {
            get => _cardType;
            private set
            {
                _cardType = value;
                ValidateProperty(nameof(CardType));
            }
        }

        public bool IsEqualTo(CardType cardType, string cardNumber, DateTime expiration)
        {
            return     _cardType == cardType
                  && _cardNumber == cardNumber
                  && _expiration == expiration;
        }
    }
}
