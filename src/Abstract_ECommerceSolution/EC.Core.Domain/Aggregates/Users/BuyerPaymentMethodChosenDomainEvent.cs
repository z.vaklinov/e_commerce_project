﻿using EC.Core.Domain.Abstraction.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EC.Core.Domain.Aggregates.Users
{
    public class BuyerPaymentMethodChosenDomainEvent : DomainEvent<Buyer>
    {
        public BuyerPaymentMethodChosenDomainEvent(PaymentMethod paymentMethod) 
            : base(paymentMethod)
        {

        }
    }
}
