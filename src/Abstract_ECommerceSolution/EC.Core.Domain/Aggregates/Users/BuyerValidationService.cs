﻿using EC.Core.Domain.Abstraction.Services;
using FluentValidation;

namespace EC.Core.Domain.Aggregates.Users
{
    public class BuyerValidationService : DomainValidationService<Buyer>
    {
        public BuyerValidationService()
        {
            RuleFor(x => x.FullName)
                .NotEmpty();
            RuleFor(x => x.PhoneNumber)
                .NotEmpty();
            
        }
    }
}
