﻿using EC.Core.Domain.Abstraction.Models;

namespace EC.Core.Domain.Aggregates.Reviews
{
    public class ReviewCreatedDomainEvent : DomainEvent<Review>
    {
        public ReviewCreatedDomainEvent(Review review)
            : base(review)
        {
        }
    }
}
