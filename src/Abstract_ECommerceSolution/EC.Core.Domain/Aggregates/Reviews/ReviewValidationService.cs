﻿using EC.Core.Domain.Abstraction.Services;
using FluentValidation;

namespace EC.Core.Domain.Aggregates.Reviews
{
    internal class ReviewValidationService : DomainValidationService<Review>
    {
        public ReviewValidationService()
        {
            RuleFor(x => x.Rating)
                .LessThanOrEqualTo(5);
        }
    }
}
