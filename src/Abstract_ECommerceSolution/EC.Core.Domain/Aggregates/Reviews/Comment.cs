﻿using EC.Core.Domain.Abstraction.Models;

namespace EC.Core.Domain.Aggregates.Reviews
{
    public class Comment : Entity<Guid>
    {
        private Guid _reviewId;
        private Guid _buyerId;
        private string _text;

        public Comment(Guid reviewId, Guid buyerId, string text)
            : this()
        {
            ReviewId = reviewId;
            BuyerId = buyerId;
            Text = text;
            AddDomainEvent(new ReviewCommentCreatedDomainEvent(this));
        }

        protected Comment()
            : base()
        {
        }

        public Guid ReviewId
        {
            get => _reviewId;
            private set
            {
                _reviewId = value;
                ValidateProperty(nameof(ReviewId));
            }
        }

        public Guid BuyerId
        {
            get => _buyerId;
            private set
            {
                _buyerId = value;
                ValidateProperty(nameof(BuyerId));
            }
        }

        public string Text
        {
            get => _text;
            private set
            {
                _text = value;
                ValidateProperty(nameof(Text));
            }
        }
    }
}
