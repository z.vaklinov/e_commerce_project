﻿using EC.Core.Domain.Abstraction.Models;

namespace EC.Core.Domain.Aggregates.Reviews
{
    public class Review : Aggregate<Guid>
    {
        private readonly List<Comment> _comments;
        private Guid _buyerId;
        private Guid _productId;
        private string _text;
        private int _rating;

        public Review(Guid buyerId, Guid productId, byte rating, string text)
            : this()
        {
            BuyerId = buyerId;
            ProductId = productId;
            Rating = rating;
            Text = text;
            AddDomainEvent(new ReviewCreatedDomainEvent(this));
        }

        protected Review()
            : base(() => new ReviewValidationService())
        {
            _comments = new List<Comment>();
        }

        public Guid BuyerId
        {
            get => _buyerId;
            private set
            {
                _buyerId = value;
                ValidateProperty(nameof(BuyerId));
            }
        }

        public Guid ProductId
        {
            get => _productId;
            private set
            {
                _productId = value;
                ValidateProperty(nameof(ProductId));
            }
        }

        public int Rating
        {
            get => _rating;
            private set
            {
                _rating = value;
                ValidateProperty(nameof(Rating));
            }
        }

        public string Text
        {
            get => _text;
            private set
            {
                _text = value;
                ValidateProperty(nameof(Text));
            }
        }

        public IReadOnlyCollection<Comment> Comments => _comments;

        public Comment AddComment(Guid reviewId, Guid buyerId, string text)
        {
            var comment = new Comment(reviewId, buyerId, text);
            _comments.Add(comment);

            return comment;
        }
    }
}
