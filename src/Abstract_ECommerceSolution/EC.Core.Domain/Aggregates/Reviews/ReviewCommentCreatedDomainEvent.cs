﻿using EC.Core.Domain.Abstraction.Models;

namespace EC.Core.Domain.Aggregates.Reviews
{
    public class ReviewCommentCreatedDomainEvent : DomainEvent<Review>
    {
        public ReviewCommentCreatedDomainEvent(
            Comment comment)
            : base(comment)
        {
        }
    }
}
