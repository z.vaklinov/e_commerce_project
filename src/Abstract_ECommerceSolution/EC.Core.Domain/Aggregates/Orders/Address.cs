﻿using EC.Core.Domain.Abstraction.Models;


namespace EC.Core.Domain.Aggregates.Orders
{
    public class Address : ValueObject
    {
        private string _street;
        private string _postCode;
        private string _city;
        private string _country;

        public Address(string street, string postCode, string city, string country)
            : this()
        {
            Street = street;
            PostCode = postCode;
            City = city;
            Country = country;

        }

        protected Address()
           : base(() => new OrderAddressValidationService())
        {
            
        }


        public string Street
        {
            get => _street;
            private set
            {
                _street = value;
                ValidateProperty(nameof(Street));
            }
        }

        public string PostCode
        {
            get => _postCode;
            private set
            {
                _postCode = value;
                ValidateProperty(nameof(PostCode));
            }
        }

        public string City
        {
            get => _city;
            private set
            {
                _city = value;
                ValidateProperty(nameof(City));
            }
        }

        public string Country
        {
            get => _country;
            private set
            {
                _country = value;
                ValidateProperty(nameof(Country));
            }
        }


        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Street;
            yield return PostCode;
            yield return City;
            yield return Country;

            // Така итерираме с фор цикъл през всяко от property-тата.

            // return new []
            // { 

            // property,
            // property...

            // }

            // Така също работи, но се задава ново място в паметта за масива.
        }
    }        
}                
