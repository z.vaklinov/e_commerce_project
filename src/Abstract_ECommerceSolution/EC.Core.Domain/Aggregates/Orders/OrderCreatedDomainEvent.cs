﻿using EC.Core.Domain.Abstraction.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EC.Core.Domain.Aggregates.Orders
{
    public class OrderCreatedDomainEvent : DomainEvent<Order>
    {
        public OrderCreatedDomainEvent(Order order)
            : base(order)
        {

        }
    }
}
