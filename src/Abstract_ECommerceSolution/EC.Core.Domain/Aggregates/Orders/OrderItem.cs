﻿using EC.Core.Domain.Abstraction.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EC.Core.Domain.Aggregates.Orders
{
    public class OrderItem : Entity<Guid>
    {
        private Guid _productId;
        private string _productName;
        private string _pictureUrl;
        private decimal _unitPrice;
        private decimal _discount;
        private int _units;

        public OrderItem(Guid productId, string productName, string pictureUrl, decimal unitPrice,
            decimal discount, int units)
            : this()
        {
            ProductId = productId;
            ProductName = productName;
            PictureUrl = pictureUrl;
            UnitPrice = unitPrice;
            Discount = discount;
            Units = units;

            if ((unitPrice * units) < discount)
            {
                throw new OrderingDomainException("The sum of the order item is lower than the applied discount");
            }

            AddDomainEvent(new OrderItemCreatedDomainEvent(this));
        }

        protected OrderItem()
            : base()
        {
        }

        public Guid ProductId
        {
            get => _productId;
            private set
            {
                _productId = value;
                ValidateProperty(nameof(ProductId));
            }
        }
        public string ProductName
        {
            get => _productName;
            private set
            {
                _productName = value;
                ValidateProperty(nameof(ProductName));
            }
        }
        public string PictureUrl
        {
            get => _pictureUrl;
            private set
            {
                _pictureUrl = value;
                ValidateProperty(nameof(PictureUrl));
            }
        }
        public decimal UnitPrice
        {
            get => _unitPrice;
            private set
            {
                _unitPrice = value;
                ValidateProperty(nameof(UnitPrice));
            }
        }
        public decimal Discount
        {
            get => _discount;
            private set
            {
                _discount = value;
                ValidateProperty(nameof(Discount));
            }
        }
        public int Units
        {
            get => _units;
            private set
            {
                _units = value;
                ValidateProperty(nameof(Units));
            }
        }

        public void SetNewDiscount(decimal discount)
        {
            if (discount < 0)
            {
                throw new OrderingDomainException("Discount is not valid");
            }

            _discount = discount;
        }

        public void AddUnits(int units)
        {
            if (units < 0)
            {
                throw new OrderingDomainException("Invalid units");
            }

            _units += units;
        }
    }
}
