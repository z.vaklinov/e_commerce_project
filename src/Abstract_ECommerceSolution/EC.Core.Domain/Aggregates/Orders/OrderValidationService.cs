﻿using EC.Core.Domain.Abstraction.Services;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EC.Core.Domain.Aggregates.Orders
{
    public class OrderValidationService : DomainValidationService<Order>
    {
        public OrderValidationService()
        {
            RuleFor(x => x.OrderDate)
                .NotEmpty();
            RuleFor(x => x.Address)
                .NotEmpty();
            RuleFor(x => x.Description)
                .NotEmpty().Length(10, 50);
        }
    }
}
