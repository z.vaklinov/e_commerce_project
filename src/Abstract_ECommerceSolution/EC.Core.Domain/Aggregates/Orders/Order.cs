﻿using EC.Core.Domain.Abstraction.Models;

namespace EC.Core.Domain.Aggregates.Orders
{
    public class Order : Aggregate<Guid>
    {
        private Guid _buyerId;
        private Guid _cartId;
        private DateTimeOffset _orderDate;
        private string _description;
        private Address _address;

        public Order(Guid buyerId, Guid cartId, string street, string postCode, string city, string country,
            DateTimeOffset orderDate, string description)
            : this()
        {
            UserId = buyerId;
            CartId = cartId;
            Address = new Address (street, postCode, city, country);
            OrderDate = orderDate;
            Description = description;

            AddDomainEvent(new OrderCreatedDomainEvent(this));
              
        }

        protected Order()
           : base(() => new OrderValidationService())
        {
            
        }

        public Guid UserId
        {
            get => _buyerId;
            private set
            {
                _buyerId = value;
                ValidateProperty(nameof(UserId));
            }
        }

        public Guid CartId
        {
            get => _cartId;
            private set
            {
                _cartId = value;
                ValidateProperty(nameof(CartId));
            }
        }

        public Address Address
        {
            get => _address;
            private set
            {
                _address = value;
                ValidateProperty(nameof(Address));
            }
        }

        public DateTimeOffset OrderDate
        {
            get => _orderDate;
            private set
            {
                _orderDate = value;
                ValidateProperty(nameof(OrderDate));
            }
        }

        public string Description
        {
            get => _description;
            private set
            {
                _description = value;
                ValidateProperty(nameof(Description));
            }
        }
    }
}
