﻿using EC.Core.Domain.Abstraction.Services;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EC.Core.Domain.Aggregates.Orders
{
    public class OrderAddressValidationService : DomainValidationService<Address>
    {
        public OrderAddressValidationService()
        {
            RuleFor(x => x.Street)
                .NotEmpty();
            RuleFor(x => x.PostCode)
                .NotEmpty();
            RuleFor(x => x.City)
                .NotEmpty();
            RuleFor(x => x.Country)
                .NotEmpty();
        }
    }
}
