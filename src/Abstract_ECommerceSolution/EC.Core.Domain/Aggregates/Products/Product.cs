﻿using EC.Core.Domain.Abstraction.Models;
using EC.Core.Domain.Aggregates.Reviews;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EC.Core.Domain.Aggregates.Products
{
    public class Product : Aggregate<Guid>
    {
        private readonly List<Review> _reviews;

        private Guid _categoryId;
        private string _name;
        private string _description;
        private int _rating;
        private int _units;
        private Price _price;


        public Product(Guid categoryId, string name, string description, Currencies currency,
            decimal ammount, int rating, int units)
            : this()
        {
            CategoryId = categoryId;
            Name = name;
            Description = description;
            Price = new Price(currency, ammount);
            Rating = rating;
            Units = units;

            AddDomainEvent(new ProductCreatedDomainEvent(this));
              
        }


        protected Product()
           : base(() => new ProductValidationService())
        {
            _reviews = new List<Review>();
        }

        public Guid CategoryId
        {
            get => _categoryId;
            private set
            {
                _categoryId = value;
                ValidateProperty(nameof(CategoryId));
            }
        }

        public string Name
        {
            get => _name;
            private set
            {
                _name = value;
                ValidateProperty(nameof(Name));
            }
        }

        public string Description
        {
            get => _description;
            private set
            {
                _description = value;
                ValidateProperty(nameof(Description));
            }
        }

        public Price Price
        {
            get => _price;
            private set
            {
                _price = value;
                ValidateProperty(nameof(Price));
            }
        }

        public int Rating
        {
            get => _rating;
            private set
            {
                _rating = value;
                ValidateProperty(nameof(Rating));
            }
        }

        public int Units
        {
            get => _units;
            private set
            {
                _units = value;
                ValidateProperty(nameof(Units));
            }
        }

        public IReadOnlyCollection<Review> Reviews => _reviews;

        public Review AddReview(Guid userId, Guid productId, byte rating, string text)
        {
            var review = new Review(userId, productId, rating, text);
            _reviews.Add(review);

            return review;
        }
    }
}
