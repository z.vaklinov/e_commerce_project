﻿using EC.Core.Domain.Abstraction.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EC.Core.Domain.Aggregates.Products
{
    public class ProductCreatedDomainEvent : DomainEvent<Product>
    {
        public ProductCreatedDomainEvent(Product product)
            : base(product)
        {

        }
    }
}
