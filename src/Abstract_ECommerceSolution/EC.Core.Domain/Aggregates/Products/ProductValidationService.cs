﻿using EC.Core.Domain.Abstraction.Services;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EC.Core.Domain.Aggregates.Products
{
    public class ProductValidationService : DomainValidationService<Product>
    {
        public ProductValidationService()
        {
            RuleFor(x => x.Name)
                .NotEmpty().Length(3, 25);
            RuleFor(x => x.Description)
                .NotEmpty().Length(10, 80);
        }
    }
}
