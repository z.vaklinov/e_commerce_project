﻿
namespace EC.Core.Domain.Aggregates.Products
{
    public enum Currencies
    {
        Euro = 1,
        Dollars = 2,
        Pounds = 3,
        Leva = 4
    }
}
