﻿using EC.Core.Domain.Abstraction.Models;

namespace EC.Core.Domain.Aggregates.Products
{
    public class Price : ValueObject
    {
        private Currencies _currency;
        private decimal _ammount;
        private static readonly decimal[] conversionRates = { 1m, 1.12m, 0.85m, 1.96m };

        public Price(Currencies currency, decimal ammount)
            : this()
        {
            Currency = currency;
            Amount = ammount;

        }

        protected Price()
            : base()
        {
        }

        public Currencies Currency
        {
            get => _currency;
            private set
            {
                _currency = value;
                ValidateProperty(nameof(Currency));
            }
        }

        public decimal Amount
        {
            get => _ammount;
            private set
            {
                _ammount = value;
                ValidateProperty(nameof(Amount));
            }
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Currency;
            yield return Amount;
        }

        public Price ConvertToCurrency(Currencies targetCurrency)
        {
            decimal fromRate = conversionRates[(int)Currency - 1];
            decimal toRate = conversionRates[(int)targetCurrency - 1];

            decimal convertedToBase = Amount / fromRate;
            decimal convertedAmount = convertedToBase * toRate;

            return new Price(targetCurrency, convertedAmount);
        }

        public Price ConvertToPrice(Currencies targetCurrency, decimal targetAmount)
        {
            decimal fromRate = conversionRates[(int)Currency - 1];
            decimal toRate = conversionRates[(int)targetCurrency - 1];

            decimal convertedAmount = targetAmount * toRate / fromRate;

            return new Price(targetCurrency, convertedAmount);
        }
    }
}
