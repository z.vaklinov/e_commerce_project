﻿using EC.Core.Application.Models;
using Microsoft.Extensions.DependencyInjection;

namespace EC.Core.Application
{
    public static class DependencyRegistrations
    {
        public static IServiceCollection RegisterApplicationDependencies(
            this IServiceCollection services)
        {
            services.AddScoped(p => new ApplicationContext(new CurrentUser(Guid.NewGuid())));

            return services;
        }
    }
}
