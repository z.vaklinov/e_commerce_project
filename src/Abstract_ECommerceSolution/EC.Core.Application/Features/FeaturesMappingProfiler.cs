﻿using AutoMapper;
using EC.Core.Application.Features.Carts.Commands.Create;
using EC.Core.Application.Features.Products.Commands.Create;
using EC.Core.Application.Features.Reviews.Commands.Create;
using EC.Core.Application.Models.DataTransfer;
using EC.Core.Domain.Aggregates.Carts;
using EC.Core.Domain.Aggregates.Products;
using EC.Core.Domain.Aggregates.Reviews;

namespace EC.Core.Application.Features
{
    public class FeaturesMappingProfiler : Profile
    {
        public FeaturesMappingProfiler()
        {
            CreateMap<Product, CreateProductCommand>().ReverseMap();
            CreateMap<Product, ProductDto>().ReverseMap();

            CreateMap<Review, CreateReviewCommand>().ReverseMap();
            CreateMap<Review, ReviewDto>().ReverseMap();

            CreateMap<Cart, CreateCartCommand>().ReverseMap();
            CreateMap<Cart, CartDto>().ReverseMap();

        }
    }
}
