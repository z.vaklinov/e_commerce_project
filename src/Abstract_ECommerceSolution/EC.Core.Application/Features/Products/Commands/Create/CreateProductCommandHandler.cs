﻿using AutoMapper;
using EC.Core.Application.Models;
using EC.Core.Application.Models.DataTransfer;
using EC.Core.Domain.Aggregates.Products;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EC.Core.Application.Features.Products.Commands.Create
{
    public class CreateProductCommandHandler
        : IRequestHandler<CreateProductCommand, ProductDto>

    {
        private readonly IMapper _mapper;

        public CreateProductCommandHandler(IMapper mapper)
        {
            _mapper = mapper;
        }

        public async Task<ProductDto> Handle(CreateProductCommand request, CancellationToken cancellationToken)
        {
            var product = _mapper.Map<Product>(request);

            //review = await _repo.AddAsync(product);

            var result = _mapper.Map<ProductDto>(product);

            return result;
        }
    }
}
