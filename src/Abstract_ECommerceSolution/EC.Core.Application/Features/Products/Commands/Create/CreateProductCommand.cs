﻿using EC.Core.Application.Models;
using EC.Core.Application.Models.DataTransfer;

namespace EC.Core.Application.Features.Products.Commands.Create
{
    public class CreateProductCommand : BaseCommand<ProductDto>
    {
        public CreateProductCommand(Guid categoryId, string name, string description, int rating, int quantity)
        {
            CategoryId = categoryId;
            Name = name;
            Description = description;
            Rating = rating;
            Quantity = quantity;
        }

        public Guid CategoryId { get; }

        public string Name { get; }

        public string Description { get; }

        public int Rating { get; }

        public int Quantity { get; }
    }
}
