﻿using AutoMapper;
using EC.Core.Application.Models.DataTransfer;
using EC.Core.Domain.Aggregates.Carts;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EC.Core.Application.Features.Carts.Commands.Create
{
    public class CreateCartCommandHandler
        : IRequestHandler<CreateCartCommand, CartDto>
    {

        private readonly IMapper _mapper;

        public CreateCartCommandHandler(IMapper mapper)
        {
            _mapper = mapper;
        }

        public async Task<CartDto> Handle(CreateCartCommand request, CancellationToken cancellationToken)
        {
            var cart = _mapper.Map<Cart>(request);

            //review = await _repo.AddAsync(product);

            var result = _mapper.Map<CartDto>(cart);

            return result;
        }
    }
}
