﻿using EC.Core.Application.Models;
using EC.Core.Application.Models.DataTransfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EC.Core.Application.Features.Carts.Commands.Create
{
    public class CreateCartCommand : BaseCommand<CartDto>
    {
        public CreateCartCommand(Guid buyerId, Guid orderId)
        {
            BuyerId = buyerId;
            OrderId = orderId;

        }

        public Guid BuyerId { get; }

        public Guid OrderId { get; }

    }
}
