﻿using AutoMapper;
using EC.Core.Application.Models;
using EC.Core.Application.Models.DataTransfer;
using EC.Core.Domain.Aggregates.Reviews;
using MediatR;

namespace EC.Core.Application.Features.Reviews.Commands.Create
{
    public class CreateReviewCommandHandler
        : IRequestHandler<CreateReviewCommand, ReviewDto>
    {
        private readonly ApplicationContext _applicationContext;
        private readonly IMapper _mapper;

        public CreateReviewCommandHandler(ApplicationContext applicationContext, IMapper mapper)
        {
            _applicationContext = applicationContext;
            _mapper = mapper;
        }

        public async Task<ReviewDto> Handle(
            CreateReviewCommand request,
            CancellationToken cancellationToken)
        {
            var review = new Review(
                _applicationContext.CurrentUser.Id,
                request.ProductId,
                request.Rating,
                request.Text);

            //review = await _repo.AddAsync(review);

            return _mapper.Map<ReviewDto>(review);
        }
    }
}
