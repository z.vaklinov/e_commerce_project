﻿using EC.Core.Application.Models;
using EC.Core.Application.Models.DataTransfer;

namespace EC.Core.Application.Features.Reviews.Commands.Create
{
    public class CreateReviewCommand : BaseCommand<ReviewDto>
    {
        public CreateReviewCommand(Guid productId, byte rating, string text)
        {
            ProductId = productId;
            Rating = rating;
            Text = text;
        }

        public Guid ProductId { get; }

        public byte Rating { get; }

        public string Text { get; }
    }
}
