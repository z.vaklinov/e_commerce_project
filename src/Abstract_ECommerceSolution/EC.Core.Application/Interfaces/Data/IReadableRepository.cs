﻿using EC.Core.Domain.Abstraction.Interfaces;

namespace EC.Core.Application.Interfaces.Data
{
    public interface IReadableRepository<TIdentificator>
        where TIdentificator : notnull
    {
        Task<IIdentifiable<TIdentificator>> GetSingleAsync(
            Func<IIdentifiable<TIdentificator>, bool> predicate,
            IReadOnlyCollection<string>? includes = null);

        Task<IReadOnlyCollection<IIdentifiable<TIdentificator>>> GetMultipleAsync(
            Func<IIdentifiable<TIdentificator>, bool> predicate,
            IReadOnlyCollection<string>? includes = null);
    }
}
