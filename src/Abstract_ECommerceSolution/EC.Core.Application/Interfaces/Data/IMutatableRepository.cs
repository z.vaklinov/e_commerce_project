﻿
using EC.Core.Domain.Abstraction.Models;

namespace EC.Core.Application.Interfaces.Data
{
    public interface IMutatableRepository<TAggregate> : IReadableRepository<Guid>
        where TAggregate : Aggregate<Guid>
    {
        Task<TAggregate> AddAsync(TAggregate aggregate);

        Task<IReadOnlyCollection<TAggregate>> AddAsync(IReadOnlyCollection<TAggregate> aggregates);

        Task<TAggregate> UpdateAsync(TAggregate aggregate);

        Task<TAggregate> DeleteAsync(TAggregate aggregate);
    }
}
