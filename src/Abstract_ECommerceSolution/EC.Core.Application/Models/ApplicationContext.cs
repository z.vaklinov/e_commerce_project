﻿namespace EC.Core.Application.Models
{
    public class ApplicationContext
    {
        public ApplicationContext(CurrentUser currentUser)
        {
            CurrentUser = currentUser;
        }

        public CurrentUser CurrentUser { get; }
    }
}
