﻿namespace EC.Core.Application.Models
{
    public class CurrentUser
    {
        public CurrentUser(Guid id)
        {
            Id = id;
        }
     
        public Guid Id { get; }
    }
}
