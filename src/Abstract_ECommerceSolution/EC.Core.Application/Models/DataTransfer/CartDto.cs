﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EC.Core.Application.Models.DataTransfer
{
    public class CartDto : BaseDto
    {
        public Guid BuyerId { get; set; }
        public Guid OrderId { get; set; }
    }
}
