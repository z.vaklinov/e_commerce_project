﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EC.Core.Application.Models.DataTransfer
{
    public class ProductDto : BaseDto
    {
        public Guid CategoryId { get; set; }

        public string Name { get; set; } = string.Empty;

        public string Description { get; set; } = String.Empty;

        public int Rating { get; set; }

        public int Units { get; set; }
    }
}
