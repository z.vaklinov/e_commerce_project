﻿namespace EC.Core.Application.Models.DataTransfer
{
    public abstract class BaseDto
    {
        public Guid Id { get; }

        public DateTimeOffset CreatedOn { get; }

        public DateTimeOffset ModifiedOn { get; }

    }
}
