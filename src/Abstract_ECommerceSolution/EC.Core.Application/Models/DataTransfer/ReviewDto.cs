﻿namespace EC.Core.Application.Models.DataTransfer
{
    public class ReviewDto : BaseDto
    {
        public Guid UserId { get; set; }

        public Guid ProductId { get; set; }

        public int Rating { get; set; }

        public string Text { get; set; } = string.Empty;
    }
}
