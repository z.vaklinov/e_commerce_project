﻿using MediatR;

namespace EC.Core.Application.Models
{
    public abstract class BaseQuery<TResult> : IRequest<TResult>
    {
    }
}
