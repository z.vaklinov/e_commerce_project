﻿using MediatR;

namespace EC.Core.Application.Models
{
    public abstract class BaseCommand<TResult> : IRequest<TResult>
    {
    }
}
