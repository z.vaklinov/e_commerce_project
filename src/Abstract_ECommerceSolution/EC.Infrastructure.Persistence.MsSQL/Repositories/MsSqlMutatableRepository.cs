﻿using EC.Core.Application.Interfaces.Data;
using EC.Core.Domain.Abstraction.Models;

namespace EC.Infrastructure.Persistence.MsSQL.Repositories
{
    internal class MsSqlMutatableRepository<TAggregate> :
        MsSqlReadableRepository<Guid>,
        IMutatableRepository<TAggregate>
        where TAggregate : Aggregate<Guid>
    {
        public MsSqlMutatableRepository(MsSQLDbContext dbContext) 
            : base(dbContext)
        {
        }

        public async Task<TAggregate> AddAsync(TAggregate aggregate)
        {
            var entry = await _dbContext
                .Set<TAggregate>()
                .AddAsync(aggregate);

            return entry.Entity;
        }

        public async Task<IReadOnlyCollection<TAggregate>> AddAsync(IReadOnlyCollection<TAggregate> aggregates)
        {
            await _dbContext
                .Set<TAggregate>()
                .AddRangeAsync(aggregates);

            return aggregates;
        }

        public async Task<TAggregate> DeleteAsync(TAggregate aggregate)
        {
            var entry = _dbContext
                .Set<TAggregate>()
                .Remove(aggregate);

            return entry.Entity;
        }

        public async Task<TAggregate> UpdateAsync(TAggregate aggregate)
        {
            var entry = _dbContext
                .Set<TAggregate>()
                .Update(aggregate);

            return entry.Entity;
        }
    }
}
