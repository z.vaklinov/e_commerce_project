﻿using EC.Core.Application.Interfaces.Data;
using EC.Core.Domain.Abstraction.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace EC.Infrastructure.Persistence.MsSQL.Repositories
{
    internal class MsSqlReadableRepository<TIdentificator> : IReadableRepository<TIdentificator>
        where TIdentificator : notnull
    {
        protected readonly MsSQLDbContext _dbContext;

        public MsSqlReadableRepository(MsSQLDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IIdentifiable<TIdentificator>> GetSingleAsync(
            Func<IIdentifiable<TIdentificator>, bool> predicate,
            IReadOnlyCollection<string>? includes = null)
        {
            var query = _dbContext
                .Set<IIdentifiable<TIdentificator>>()
                .AsQueryable();

            if(includes != null)
            {
                foreach (var include in includes)
                {
                    query = query.Include(include);
                }
            }

            var result = query
                .Where(predicate)
                .FirstOrDefault();

            return result;
        }

        public Task<IReadOnlyCollection<IIdentifiable<TIdentificator>>> GetMultipleAsync(
            Func<IIdentifiable<TIdentificator>, bool> predicate,
            IReadOnlyCollection<string>? includes = null)
        {
            throw new NotImplementedException();
        }
    }
}
