﻿using EC.Core.Application.Interfaces.Data;
using EC.Infrastructure.Persistence.MsSQL.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace EC.Infrastructure.Persistence.MsSQL
{
    public static class DependencyRegistrations
    {
        public static IServiceCollection RegisterMsSQLDependencies(
            this IServiceCollection services,
            string mssqlConnectionString)
        {
            services.AddDbContext<MsSQLDbContext>(
                builder => builder.UseSqlServer(
                    mssqlConnectionString,
                    opt => opt.MigrationsAssembly(typeof(MsSQLDbContext).Assembly.GetName().Name)));

            services.AddTransient(typeof(IReadableRepository<>), typeof(MsSqlReadableRepository<>));
            services.AddTransient(typeof(IMutatableRepository<>), typeof(MsSqlMutatableRepository<>));

            return services;
        }
    }
}
