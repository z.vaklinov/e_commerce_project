﻿namespace EC.Infrastructure.Persistence.MsSQL.Models
{
    internal abstract class DataModel
    {
        public bool IsDeleted { get; set; }

        public DateTime? DeletedOn { get; set; }
    }
}
