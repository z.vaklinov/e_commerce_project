﻿using EC.Core.Domain.Aggregates.Reviews;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EC.Infrastructure.Persistence.MsSQL.Configurations.Reviews
{
    internal class CommentConfiguration : BaseEntityConfiguration<Comment>
    {
        public override void Configure(EntityTypeBuilder<Comment> builder)
        {
            base.Configure(builder);
        }
    }
}
