﻿using EC.Core.Domain.Aggregates.Reviews;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EC.Infrastructure.Persistence.MsSQL.Configurations.Reviews
{
    internal class ReviewConfiguration : BaseEntityConfiguration<Review>
    {
        public override void Configure(EntityTypeBuilder<Review> builder)
        {
            base.Configure(builder);

            builder
                .HasMany(x => x.Comments)
                .WithOne()
                .HasPrincipalKey(p => p.Id)
                .HasForeignKey(c => c.ReviewId);
        }
    }
}
