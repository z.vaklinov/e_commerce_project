﻿using EC.Core.Domain.Abstraction.Models;
using EC.Infrastructure.Persistence.MsSQL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pluralize.NET;

namespace EC.Infrastructure.Persistence.MsSQL.Configurations
{
    internal abstract class BaseEntityConfiguration<TEntity> : IEntityTypeConfiguration<TEntity>
        where TEntity : Entity<Guid>
    {
        protected readonly Pluralizer Pluralizer = new Pluralizer();

        public virtual void Configure(EntityTypeBuilder<TEntity> builder)
        {
            builder.ToTable(Pluralizer.Pluralize(typeof(TEntity).Name));

            builder
                .Property<bool>(nameof(DataModel.IsDeleted))
                .IsRequired(true)
                .HasDefaultValue(false);

            builder
                .Property<DateTime?>(nameof(DataModel.DeletedOn))
                .IsRequired(false);

            builder
                .HasQueryFilter(x =>
                    EF.Property<bool>(x, nameof(DataModel.IsDeleted)) == false);

            builder.HasKey(x => x.Id);

            builder
                .Property(x => x.Id)
                .ValueGeneratedOnAdd();

            builder
                .Ignore(x => x.DomainEvents);
        }
    }
}
