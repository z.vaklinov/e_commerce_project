﻿using EC.Core.Application.Interfaces.Data;
using EC.Core.Application.Models;
using MediatR;
using System.Data;

namespace EC.Infrastructure.Persistence
{
    internal class TransactionPipelineService<TRequest, TResponse>        : 
        IPipelineBehavior<TRequest, TResponse>
        where TRequest : IRequest<TResponse>
    {
        private readonly IDataUnitOfWork _data;

        public TransactionPipelineService(IDataUnitOfWork data)
        {
            _data = data;
        }

        public async Task<TResponse> Handle(
            TRequest request,
            CancellationToken cancellationToken,
            RequestHandlerDelegate<TResponse> next)
        {
            if (request is BaseCommand<TResponse> == false)
            {
                return await next();
            }

            try
            {
                await _data.BeginTransactionAsync(IsolationLevel.ReadCommitted, cancellationToken);
                var result = await next();
                await _data.CommitTransactionAsync(cancellationToken);

                return result;
            }
            catch
            {
                await _data.RollbackTransactionAsync();
                throw;
            }
        }
    }
}
