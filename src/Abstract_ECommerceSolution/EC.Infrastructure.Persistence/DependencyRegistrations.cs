﻿using EC.Core.Application.Interfaces.Data;
using EC.Infrastructure.Persistence.MsSQL;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace EC.Infrastructure.Persistence
{
    public static class DependencyRegistrations
    {
        public static IServiceCollection RegisterPersistenceDependencies(
            this IServiceCollection services,
            string mssqlConnectionString)
        {
            services.RegisterMsSQLDependencies(mssqlConnectionString);

            services.AddTransient<IDataUnitOfWork, DataUnitOfWork>();
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(TransactionPipelineService<,>));

            return services;
        }
    }
}
