﻿using EC.Infrastructure.Logging.Services;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace EC.Infrastructure.Logging
{
    public static class DependencyRegistrations
    {
        public static IServiceCollection RegisterLoggingDependencies(
            this IServiceCollection services)
        {
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(CommandLoggingPipelineService<,>));

            return services;
        }
    }
}