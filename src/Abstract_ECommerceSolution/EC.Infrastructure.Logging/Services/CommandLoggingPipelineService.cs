﻿using EC.Core.Application.Models;
using MediatR;
using Microsoft.Extensions.Logging;

namespace EC.Infrastructure.Logging.Services
{
    public class CommandLoggingPipelineService<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
         where TRequest : IRequest<TResponse>
    {
        private readonly ILogger<CommandLoggingPipelineService<TRequest, TResponse>> _logger;

        public CommandLoggingPipelineService(ILogger<CommandLoggingPipelineService<TRequest, TResponse>> logger)
        {
            _logger = logger;
        }

        public async Task<TResponse> Handle(
            TRequest request, 
            CancellationToken cancellationToken,
            RequestHandlerDelegate<TResponse> next)
        {
            if(request.GetType().IsSubclassOf(typeof(BaseCommand<TResponse>)) == false)
            {
                return await next();
            }

            var commandName = request.GetType().Name;

            try
            {
                _logger.LogInformation($"Handling {commandName} now.");
                var response = await next();
                _logger.LogInformation($"Just handled {commandName}.");

                return response;
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"The command {commandName} has failed.");
                throw;
            }
        }
    }
}
